using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using ManyTasksInTheAir.Controller;

namespace ManyTasksInTheAir
{
	public partial class TaskListScreen : DialogViewController
	{
		public TaskListScreen () : base (UITableViewStyle.Plain, null)
		{
			refreshList ();
		}

		public event EventHandler taskClicked;
		public void refreshList()
		{
			var root = new RootElement("");
			Section todaySection = new Section ("NOW");
			Section futureSection = new Section ("FUTURE");
			TableView.BackgroundColor = UIColor.FromRGBA (30, 30, 30, 0);
			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;

			bool showToday = false, showFuture = false;

			foreach (var task in TaskController.getTaskList())
			{
				var taskViewController = new TaskCellViewController (task);
				taskViewController.TaskSelected += (sender, e) => 
				{
					if(taskClicked != null)
						taskClicked(task, null);
				};

				UIViewElement taskElementView = new UIViewElement (null, taskViewController.View, true);
//				taskElementView. += delegate
//				{
//					if(taskClicked != null)
//						taskClicked(task, null);
//				};

				if (task.DueDate.Date <= DateTime.Now.Date)
				{
					todaySection.Add (taskElementView);
					showToday = true;
				} 
				else
				{
					futureSection.Add (taskElementView);
					showFuture = true;
				}
			}

			if(showToday)
				root.Add (todaySection);

			if(showFuture)
				root.Add (futureSection);

			Root = root;
		}
	}
}
