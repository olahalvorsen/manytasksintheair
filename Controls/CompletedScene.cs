using System;
using MonoTouch.SpriteKit;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace ManyTasksInTheAir
{
	public class CompletedScene : SKScene
	{
		SKSpriteNode star;
		public CompletedScene (SizeF size) : base(size)
		{
			star = SKSpriteNode.FromImageNamed ("motivationStar.png");
			star.Position = new PointF (Size.Width / 2, Size.Height / 2);
			AddChild (star);

			star.RunAction(SKAction.RepeatActionForever(SKAction.RotateByAngle (10, 4)), () => {
				star.RemoveAllActions ();
			});

			star.RunAction(SKAction.RepeatActionForever(SKAction.ResizeTo (new SizeF(star.Size.Width*2, star.Size.Height*2), 1)), () => {
				star.RemoveAllActions ();

			});

			//SKEmitterNode.
			var particleSystem = NSKeyedUnarchiver.UnarchiveFile ("snow.sks") as SKEmitterNode;
			particleSystem.Position = new PointF (this.Frame.Width * .5f, this.Frame.Height);
			this.AddChild (particleSystem);

			SKLabelNode motLabel = new SKLabelNode("Verdana");
			motLabel.FontSize = 20F;
			motLabel.Position = new PointF (UIScreen.MainScreen.Bounds.Width/2, UIScreen.MainScreen.Bounds.Height/3);
			//motLabel.HorizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center;
			//motLabel.VerticalAlignmentMode = SKLabelVerticalAlignmentMode.Center;
			motLabel.Text = Utils.getMotivationString ();
			motLabel.FontColor = UIColor.White;
			this.AddChild (motLabel);
		}

//		public override void TouchesBegan (NSSet touches, UIEvent evt)
//		{
//			base.TouchesBegan (touches, evt);
//			var touch = (UITouch)touches.AnyObject;
//			AnimateMonkey (touch.LocationInNode (this));
//		}
//		void AnimateMonkey (PointF location)
//		{
//			//star.RunAction (animate);
//			star.RunAction (SKAction.RotateByAngle (location, 1), () => {
//				star.RemoveAllActions ();
//			});
//		}
	}
}

