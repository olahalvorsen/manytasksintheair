using System;

namespace ManyTasksInTheAir
{
	public static class Utils
	{
		public static Random rand;

		public static string getMotivationString()
		{
			if(rand == null)
				rand = new Random((int)DateTime.Now.Ticks);

			string [] motivationStrings = new string[]
			{
				"Good work, keep it up!",
				"WOooohoo!",
				"Nice one",
				"Another one down! :D",
				"Task Completed! Great!",
				"Fantastic!",
				"Awesome!",
				"YEAH!",
				"Super!!!!",
				"Great stuff",
				"Here's a spinning star! ;)",
			};


			return motivationStrings[rand.Next(0, motivationStrings.Length-1)];
		}

		public static string NeatDateTimeString(DateTime currDate)
		{

			if (currDate == DateTime.MinValue)
				return "NOW";
			else if (currDate.Date == DateTime.Now.Date.AddDays(-1.0))
				return "Yesterday at " + currDate.ToShortTimeString();
			else if (currDate.Date == DateTime.Now.Date)
				return "Today at " + currDate.ToShortTimeString();
			else if (currDate.Date == DateTime.Now.Date.AddDays(1.0))
				return "Tomorrow at "+  currDate.ToShortTimeString();
			else
				return currDate.Day + "/" + currDate.Month + "/" + currDate.Year + " at " + currDate.ToShortTimeString();
		}
	}
}

