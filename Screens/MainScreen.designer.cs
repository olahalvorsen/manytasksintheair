// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ManyTasksInTheAir
{
	[Register ("MainScreen")]
	partial class MainScreen
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnPostpone { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnTaskCompleted { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnViewTaskNow { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView imgMotivation { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblFutureTasks { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblPostponeTime { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISlider sliderPostponeTime { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel txtMotication { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel txtTaskTitle { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnPostpone != null) {
				btnPostpone.Dispose ();
				btnPostpone = null;
			}

			if (btnTaskCompleted != null) {
				btnTaskCompleted.Dispose ();
				btnTaskCompleted = null;
			}

			if (btnViewTaskNow != null) {
				btnViewTaskNow.Dispose ();
				btnViewTaskNow = null;
			}

			if (lblFutureTasks != null) {
				lblFutureTasks.Dispose ();
				lblFutureTasks = null;
			}

			if (lblPostponeTime != null) {
				lblPostponeTime.Dispose ();
				lblPostponeTime = null;
			}

			if (sliderPostponeTime != null) {
				sliderPostponeTime.Dispose ();
				sliderPostponeTime = null;
			}

			if (txtTaskTitle != null) {
				txtTaskTitle.Dispose ();
				txtTaskTitle = null;
			}

			if (txtMotication != null) {
				txtMotication.Dispose ();
				txtMotication = null;
			}

			if (imgMotivation != null) {
				imgMotivation.Dispose ();
				imgMotivation = null;
			}
		}
	}
}
