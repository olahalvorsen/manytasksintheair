using System;
using MonoTouch.UIKit;
using ManyTasksInTheAir.Model;
using ManyTasksInTheAir.Controller;

namespace ManyTasksInTheAir
{
	public static class NotificationsHandler
	{
		public static void recreateAllNotifications()
		{
			UIApplication.SharedApplication.CancelAllLocalNotifications();

			foreach (SymTask task in TaskController.getTaskList())
			{
				if (task.DueDate < DateTime.Now)
					continue;

				UILocalNotification notification = new UILocalNotification();
				notification.FireDate = task.DueDate;
				notification.AlertAction = "View Task";
				notification.AlertBody = task.Title;
				notification.SoundName = UILocalNotification.DefaultSoundName;
				notification.ApplicationIconBadgeNumber = 1;

				UIApplication.SharedApplication.ScheduleLocalNotification(notification);
			}

		}
	}
}

