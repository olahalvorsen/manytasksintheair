using System;
using SQLite;

namespace ManyTasksInTheAir.Model
{
	public class SymTask
	{
		[PrimaryKey]
		public string TaskID { get; set; }
		public string Title { get; set; }
		public DateTime DueDate { get; set; }
	
		public SymTask()
		{
			if(string.IsNullOrEmpty(TaskID))
				TaskID = Guid.NewGuid ().ToString ();
		}
	}
}

