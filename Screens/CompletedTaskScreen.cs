using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.SpriteKit;
using System.Threading.Tasks;

namespace ManyTasksInTheAir
{
	public partial class CompletedTaskScreen : UIViewController
	{
		public CompletedTaskScreen () : base ("CompletedTaskScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void LoadView ()
		{
			base.LoadView ();
			View = new SKView {
				ShowsFPS = false,
				ShowsNodeCount = false,
				ShowsDrawCount = false
			};
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
			var view = (SKView)View;
			if (view.Scene == null) {
				var scene = new CompletedScene (View.Bounds.Size);
				scene.ScaleMode = SKSceneScaleMode.AspectFill;
				scene.BackgroundColor = UIColor.FromRGBA(30,30,30,40);
				this.View.BackgroundColor = UIColor.FromRGBA (30, 30, 30, 0);
				view.PresentScene (scene);
			}
		}

		async public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			await Task.Delay (5000);
			DismissViewController (true, null);
			// Perform any additional setup after loading the view, typically from a nib.
		}
	}
}

