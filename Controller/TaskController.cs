using System;
using SQLite;
using System.Collections.Generic;
using ManyTasksInTheAir.Model;
using System.Text;

namespace ManyTasksInTheAir.Controller
{
	public static class TaskController
	{
		#region CRUD operations

		public static SymTask createTask (string taskName) 
		{
			SymTask newTask = new SymTask { Title = taskName};

			LocalDBController.dbConn.Insert (newTask);
			return newTask;
		}

		public static void deleteTask (SymTask taskToDelete)
		{
			LocalDBController.dbConn.Delete (taskToDelete);
		}

		public static void updateTask (SymTask taskToUpdate)
		{
			LocalDBController.dbConn.Update (taskToUpdate);
			NotificationsHandler.recreateAllNotifications ();
		}

		#endregion

		#region Queries

		public static SymTask getFirstTaskWhichIsDue(bool force)
		{
			var query = LocalDBController.dbConn.Table<SymTask> ().OrderBy(f => f.DueDate);
			foreach (var t in query) {
				if (force)
					return t;

				if (t.DueDate < DateTime.Now)
					return t;
				else
					return null;
			}

			return null;
		}

		public static List<SymTask> getTaskList()
		{
			List<SymTask> listOfTasks = new List<SymTask> ();

			var query = LocalDBController.dbConn.Table<SymTask> ().OrderBy(f => f.DueDate);
			foreach (var t in query) {
				listOfTasks.Add (t);
			}

			return listOfTasks;
		}

		public static string getNextFutureTaskDescription ()
		{
			var query = LocalDBController.dbConn.Table<SymTask> ().OrderBy(f => f.DueDate);
			SymTask firstTask = null;
			int count = 0;
			foreach (var t in query) {
				if (firstTask == null)
					firstTask = t;

				count++;
			}

			if (count > 0)
				return count + " tasks in the air! Next task arriving in" + timeUntil (firstTask.DueDate);

			return "No tasks in the air, why don't you create one?";
		}


		static string timeUntil (DateTime dueDate)
		{
			TimeSpan ts = dueDate - DateTime.Now;
//			StringBuilder sr = new StringBuilder ();
			if (ts.Days > 1)
				return " " + ts.Days + " Days";
			else if (ts.Hours > 1)
				return " " + ts.Hours + " Hours";
			else if (ts.Minutes > 1)
				return " " + ts.Minutes + " Minutes";
			else if (ts.Minutes == 1)
				return " " + ts.Minutes + " Minute";
			else
				return " less than a minute";
	
		}
		#endregion

	}
}

