using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ManyTasksInTheAir.Controller;
using ManyTasksInTheAir.Model;
using System.Threading.Tasks;
using GoogleAdMobAds;
using MonoTouch.AudioToolbox;
using MonoTouch.SpriteKit;

namespace ManyTasksInTheAir
{
	public partial class MainScreen : UIViewController
	{
		const string AdmobID = "ca-app-pub-7483002594208211/5954273813";
		GADBannerView adView;
		bool viewOnScreen = false;
		UIButton btnAddTask, btnShowList;
		DateTime postponeTime;
		RectangleF taskTitleFrameVisible, taskTitleFrameInvisible;
		private SymTask _CurrentTask;
		UIScrollView scrollView;
		TaskListScreen taskListScreen;

		private bool _ShowList;
		public bool ShowList
		{
			get
			{
				return _ShowList;
			}
			set
			{
				_ShowList = value;

				if (value)
				{
					taskListScreen.refreshList ();
					scrollView.SetContentOffset(new PointF(0, 0), true);

					if(TaskController.getTaskList().Count == 0)
						lblFutureTasks.Alpha = 1F;
					else
						lblFutureTasks.Alpha = 0F;

					txtTaskTitle.Alpha = 0F;
					txtMotication.Alpha = 0F;
					imgMotivation.Alpha = 0F;

					btnTaskCompleted.Alpha = 0F;
					sliderPostponeTime.Alpha = 0F;
					lblPostponeTime.Alpha = 0F;
					btnPostpone.Alpha = 0F;

				
					btnViewTaskNow.Alpha = 0F;


					UIView.Animate (
						duration: 0.5,
						delay:0,
						options: UIViewAnimationOptions.CurveEaseOut,
						animation: () =>
						{
							taskListScreen.View.Frame = new RectangleF (0, 80, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height - 100);
						}, completion: () =>
						{
						});
				}
				else
				{
					scrollView.SetContentOffset(new PointF(0, scrollView.ContentSize.Height - scrollView.Bounds.Size.Height), true);
					txtTaskTitle.Alpha = 1F;
					CurrentTask = CurrentTask;

					UIView.Animate (
						duration: 1,
						delay:0,
						options: UIViewAnimationOptions.CurveEaseInOut,
						animation: () =>
						{
							taskListScreen.View.Frame = new RectangleF (0, -2272, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height - 100);

						}, completion: () =>
						{
						});


				}
			}
		}

		public SymTask CurrentTask
		{
			get
			{
				return _CurrentTask;
			}
			set
			{
				_CurrentTask = value;
				if (_CurrentTask != null)
				{
					//SystemSound.FromFile ("woosh.wav").PlaySystemSound ();
					txtTaskTitle.Text = _CurrentTask.Title;
					UIView.Animate (
						duration: 1,
						delay:0,
						options: UIViewAnimationOptions.CurveEaseOut,
						animation: () =>
						{
							txtTaskTitle.Frame = taskTitleFrameVisible;
							btnTaskCompleted.Alpha = 1F;
							sliderPostponeTime.Alpha = 1F;
							lblPostponeTime.Alpha = 1F;
							btnPostpone.Alpha = 1F;

							txtMotication.Alpha = 0F;
							imgMotivation.Alpha = 0F;
							lblFutureTasks.Alpha = 0F;
							btnViewTaskNow.Alpha = 0F;
							if(adView != null)
							{
								adView.Alpha = 0F;
							}
						}
						, completion: () =>
					{
							UIView.Animate (
								duration: 4,
								delay: 0,
								options: UIViewAnimationOptions.Autoreverse | UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.Repeat,
							animation: () =>
							{
									txtTaskTitle.Frame = new RectangleF(taskTitleFrameVisible.X + 5, taskTitleFrameVisible.Y + 20, taskTitleFrameVisible.Width, taskTitleFrameVisible.Height);
								}, completion: null
							);

					});



				} else
				{
					UIView.Animate (
						duration: 0.7,
						animation: () =>
						{
							txtTaskTitle.Frame = taskTitleFrameInvisible;

							btnTaskCompleted.Alpha = 0F;
							sliderPostponeTime.Alpha = 0F;
							lblPostponeTime.Alpha = 0F;
							btnPostpone.Alpha = 0F;

							lblFutureTasks.Alpha = 1F;

							txtMotication.Alpha = 0F;
							imgMotivation.Alpha = 0F;

							//GADRequest.Request.TestDevices = new string[]{ "534c2ad025deb4cc5df762f6f2e4eaf7" };
							if(adView != null)
							{
								adView.LoadRequest (GADRequest.Request);
								adView.Alpha = 1F;
							}

							lblFutureTasks.Text = TaskController.getNextFutureTaskDescription ();

							if(lblFutureTasks.Text.StartsWith("No tasks"))
								btnViewTaskNow.Alpha = 0F;
							else
								btnViewTaskNow.Alpha = 1F;
						}
						, completion: () =>
					{

					});
				}
			}
		}

		public MainScreen () : base ("MainScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			txtMotication.Alpha = 0F;
			imgMotivation.Alpha = 0F;

			btnTaskCompleted.Alpha = 0F;
			sliderPostponeTime.Alpha = 0F;
			lblPostponeTime.Alpha = 0F;
			btnPostpone.Alpha = 0F;

			lblFutureTasks.Alpha = 0F;
			btnViewTaskNow.Alpha = 0F;
			btnViewTaskNow.Alpha = 0F;


			txtTaskTitle.Text = "";

			adView = new GADBannerView (size: GADAdSizeCons.Banner, origin: new PointF (0, UIScreen.MainScreen.Bounds.Height - GADAdSizeCons.Banner.size.Height)) {
				AdUnitID = AdmobID,
				RootViewController = this
			};

			adView.DidReceiveAd += (sender, args) =>
			{
				if (!viewOnScreen)
					View.AddSubview (adView);
				viewOnScreen = true;
			};
		
			scrollView = new UIScrollView (UIScreen.MainScreen.Bounds);
			scrollView.Bounces = false;
			//scrollView.BouncesZoom = true;
			scrollView.ScrollEnabled = false;
			scrollView.ShowsVerticalScrollIndicator = false;



			View.InsertSubview (scrollView, 0);


			UIImageView backgroundView;
			bool isIPhone5 = (UIScreen.MainScreen.Bounds.Size.Height > 480.0);
			if (isIPhone5) {
				backgroundView = new UIImageView (UIImage.FromBundle ("backgroundIphone5"));
			} else {
				backgroundView = new UIImageView (UIImage.FromBundle ("backgroundIphone4"));
			} 

			scrollView.ContentSize = backgroundView.Image.Size;
			scrollView.AddSubview (backgroundView);


			scrollView.SetContentOffset(new PointF(0, scrollView.ContentSize.Height - scrollView.Bounds.Size.Height), true);


			taskTitleFrameVisible = txtTaskTitle.Frame;
			taskTitleFrameInvisible = new RectangleF (txtTaskTitle.Frame.X, -200, txtTaskTitle.Frame.Width, txtTaskTitle.Frame.Height);

			btnAddTask = new UIButton (UIButtonType.ContactAdd);
			btnAddTask.Frame = new RectangleF (UIScreen.MainScreen.Bounds.Width - 50, 20, 50, 50);
			btnAddTask.TouchUpInside += (sender, e) =>
			{
				EditTaskScreen editTaskScreen = new EditTaskScreen ();
				NavigationController.PushViewController (editTaskScreen, true);
			};
			View.Add (btnAddTask);

			btnShowList = new UIButton (UIButtonType.DetailDisclosure);
			btnShowList.Frame = new RectangleF (0, 20, 50, 50);
			btnShowList.TouchUpInside += (sender, e) =>
			{
				if(!ShowList)
					ShowList = true;
				else
					ShowList = false;
			};
			View.Add (btnShowList);


			lblPostponeTime.Text = "In about a day";
			postponeTime = DateTime.Now.AddHours (24);
			sliderPostponeTime.ValueChanged += (sender, e) =>
			{
				if (sliderPostponeTime.Value < 1)
				{
					lblPostponeTime.Text = "In a few minutes";
				} else if (sliderPostponeTime.Value < 2)
				{
					lblPostponeTime.Text = "In about half an hour";
				} else if (sliderPostponeTime.Value < 3)
				{
					lblPostponeTime.Text = "In about an hour";
				} else if (sliderPostponeTime.Value < 4)
				{
					lblPostponeTime.Text = "In a few hours";
				} else if (sliderPostponeTime.Value < 5)
				{
					lblPostponeTime.Text = "In about half a day";
				} else if (sliderPostponeTime.Value < 6)
				{
					lblPostponeTime.Text = "In about a day";
				} else if (sliderPostponeTime.Value < 7)
				{
					lblPostponeTime.Text = "In a couple of days";
				} else if (sliderPostponeTime.Value < 8)
				{
					lblPostponeTime.Text = "In about a week";
				} else if (sliderPostponeTime.Value < 9)
				{
					lblPostponeTime.Text = "In a couple of weeks";
				} else if (sliderPostponeTime.Value <= 10)
				{
					lblPostponeTime.Text = "In a month!";
				}
			};

			btnPostpone.TouchUpInside += async (sender, e) =>
			{	 
				if (sliderPostponeTime.Value < 1)
				{
					postponeTime = DateTime.Now.AddMinutes (sliderPostponeTime.Value * 10);
				} else if (sliderPostponeTime.Value < 2)
				{
					postponeTime = DateTime.Now.AddMinutes (30);
				} else if (sliderPostponeTime.Value < 3)
				{
					postponeTime = DateTime.Now.AddHours (1);
				} else if (sliderPostponeTime.Value < 4)
				{
					postponeTime = DateTime.Now.AddHours (3);
				} else if (sliderPostponeTime.Value < 5)
				{
					postponeTime = DateTime.Now.AddHours (12);
				} else if (sliderPostponeTime.Value < 6)
				{
					postponeTime = DateTime.Now.AddHours (24);
				} else if (sliderPostponeTime.Value < 7)
				{
					postponeTime = DateTime.Now.AddDays (2);
				} else if (sliderPostponeTime.Value < 8)
				{
					postponeTime = DateTime.Now.AddDays (5);
				} else if (sliderPostponeTime.Value < 9)
				{
					postponeTime = DateTime.Now.AddDays (12);
				} else if (sliderPostponeTime.Value <= 10)
				{
					postponeTime = DateTime.Now.AddDays (28);
				}

				if (CurrentTask != null)
				{
					CurrentTask.DueDate = postponeTime;
					TaskController.updateTask (CurrentTask);
					SystemSound.FromFile ("woosh.wav").PlaySystemSound ();
					CurrentTask = null;
					await Task.Delay (1000);
					CurrentTask = TaskController.getFirstTaskWhichIsDue (false);
				}
			};

			btnTaskCompleted.TouchUpInside += (sender, e) =>
			{
				if (CurrentTask != null)
				{
					CompletedTaskScreen cts = new CompletedTaskScreen();
					PresentViewController(cts, true, null);
					TaskController.deleteTask (CurrentTask);
					SystemSound.FromFile ("tada.wav").PlayAlertSound ();
//
//					UIView.Animate (
//						duration: 0.5,
//						animation: () =>
//						{
//							txtTaskTitle.Frame = taskTitleFrameInvisible;
//
//							btnTaskCompleted.Alpha = 0F;
//							sliderPostponeTime.Alpha = 0F;
//							lblPostponeTime.Alpha = 0F;
//							btnPostpone.Alpha = 0F;
//
//							lblFutureTasks.Alpha = 0F;
//							btnViewTaskNow.Alpha = 0F;
//
//							txtMotication.Alpha = 1F;
//							imgMotivation.Alpha = 1F;
//
//							txtMotication.Text = Utils.getMotivationString();
//
//							//GADRequest.Request.TestDevices = new string[]{ "534c2ad025deb4cc5df762f6f2e4eaf7" };
//							if(adView != null)
//							{
//								adView.LoadRequest (GADRequest.Request);
//								adView.Alpha = 0F;
//							}
//						}
//						, completion: async () =>
//						{
//							await Task.Delay (5000);
//							CurrentTask = TaskController.getFirstTaskWhichIsDue (false);
//						});
				}
			};

			btnViewTaskNow.TouchUpInside += (sender, e) =>
			{
				CurrentTask = TaskController.getFirstTaskWhichIsDue (true);
			};

			taskListScreen = new TaskListScreen ();
			taskListScreen.taskClicked += (sender, e) => 
			{
				CurrentTask = sender as SymTask;
				ShowList = false;
			};
			//ShowList = false;
			View.Add (taskListScreen.View);

			CurrentTask = TaskController.getFirstTaskWhichIsDue (false);
			StartTimer ();
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
		}

		async void StartTimer ()
		{
			while (true)
			{
				await Task.Delay (15000);
				if (CurrentTask == null && ShowList == false)
				{
					CurrentTask = TaskController.getFirstTaskWhichIsDue (false);
					if(CurrentTask != null)
						SystemSound.FromFile ("woosh.wav").PlaySystemSound ();
				}
			}
		}

		public void somemethog()
		{

		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			//NavigationItem.SetRightBarButtonItem(btnAddTask, true);
			NavigationController.NavigationBarHidden = true;
			CurrentTask = TaskController.getFirstTaskWhichIsDue (false);
			ShowList = false;
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			NavigationController.NavigationBarHidden = false;

		}
	}
}

