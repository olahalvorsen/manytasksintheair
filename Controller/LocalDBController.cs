using System;
using SQLite;
using ManyTasksInTheAir.Model;

namespace ManyTasksInTheAir.Controller
{
	public static class LocalDBController
	{
		public static SQLiteConnection dbConn;

		public static void setupConnection()
		{
			string folder = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			dbConn = new SQLiteConnection (System.IO.Path.Combine (folder, "airTasks.db"));
			setupDBTables ();
		}

		private static void setupDBTables()
		{
			dbConn.CreateTable<SymTask>();
		}
	}
}

