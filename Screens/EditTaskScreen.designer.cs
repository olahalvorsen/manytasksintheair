// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ManyTasksInTheAir
{
	[Register ("EditTaskScreen")]
	partial class EditTaskScreen
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnCreateTask { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblInstruction { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtTaskTitle { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnCreateTask != null) {
				btnCreateTask.Dispose ();
				btnCreateTask = null;
			}

			if (lblInstruction != null) {
				lblInstruction.Dispose ();
				lblInstruction = null;
			}

			if (txtTaskTitle != null) {
				txtTaskTitle.Dispose ();
				txtTaskTitle = null;
			}
		}
	}
}
