using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ManyTasksInTheAir.Model;

namespace ManyTasksInTheAir
{
	public partial class TaskCellViewController : UIViewController
	{
		private SymTask _CurrentTask;
		public SymTask CurrentTask
		{
			get
			{
				return _CurrentTask;
			}
			set
			{
				_CurrentTask = value;
			}
		}

		public TaskCellViewController (SymTask pTask) : base ("TaskCellViewController", null)
		{
			CurrentTask = pTask;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public event EventHandler TaskSelected;
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			lblTitle.Text = CurrentTask.Title;
			lblTime.Text = Utils.NeatDateTimeString(CurrentTask.DueDate);
			// Perform any additional setup after loading the view, typically from a nib.

			UITapGestureRecognizer rec = new UITapGestureRecognizer ();
			rec.AddTarget (() =>
				{
					if(TaskSelected != null)
						TaskSelected(CurrentTask, null);
				});
			this.View.AddGestureRecognizer (rec);
		}


	}
}

