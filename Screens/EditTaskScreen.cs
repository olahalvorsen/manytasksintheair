using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ManyTasksInTheAir.Controller;

namespace ManyTasksInTheAir
{
	public partial class EditTaskScreen : UIViewController
	{
		public EditTaskScreen () : base ("EditTaskScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			txtTaskTitle.BecomeFirstResponder();
			txtTaskTitle.ShouldReturn += (textField) => 
			{
				createTask();
				return true;
			};

			btnCreateTask.TouchUpInside += (sender, e) => 
			{
				createTask();
			};

			// Perform any additional setup after loading the view, typically from a nib.
		}

		private void createTask()
		{
			if(!string.IsNullOrEmpty(txtTaskTitle.Text))
			{
				TaskController.createTask(txtTaskTitle.Text);
				txtTaskTitle.Text = "";
				NavigationController.PopViewControllerAnimated(true);
			}
			else
			{
				new UIAlertView("Enter a Task", "Write something in the textbox to create a task", null, "Ok", null).Show();
			}
		}
	}


}

